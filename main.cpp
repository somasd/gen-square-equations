#include <iostream>
#include <sstream>
using namespace std;

void print_with_sign(int num);

int main(int argc, char **argv)
{
    if(argc < 3)
    {
        cout << "Using: " << argv[0] << " <min> <max>" << endl;
        return 1;
    }
    int min, max;
    stringstream(argv[1]) >> min;
    stringstream(argv[2]) >> max;
    if(min > max)
    {
        min += max;
        max = min - max;
        min -= max;
    }

    srand(time(0));

    int x1 = min + rand() % (max - min);
    int x2 = min + rand() % (max - min);

    int a_param = 0;
    while(a_param == 0)
        a_param = min + rand() % (max - min);

    //cout << "a = " << a_param << endl;
    cout << "x1 = " << x1 << endl;
    cout << "x2 = " << x2 << endl;
    cout << endl;


    if(a_param == 1)
        cout << "x^2";
    else if(a_param == -1)
        cout << "-(x^2)";
    else
        cout << a_param << "x^2";

    int b_param = a_param * (x1 + x2) * -1;
    if(x1 != x2 * -1)
    {
        cout << " ";
        print_with_sign(b_param);
        cout << "x";
    }

    int c_param = a_param * (x1 * x2);
    if(x1 != 0 && x2 != 0)
    {
        cout << " ";
        print_with_sign(c_param);
    }
    cout << " = 0" << endl;

    return 0;
}

void print_with_sign(int num)
{
    if(num < 0)
        cout << "- " << num * -1;
    else if(num > 0)
        cout << "+ " << num;
    else
        cout << "+ 0";

}
